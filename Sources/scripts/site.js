// HTTPS Redirect 
------------------------------------------
if(location.protocol == 'http:') {
  location.replace(location.href.replace(/http:/, 'https:'));
}

// Legacy Browser Msg -----------------------
var agent = navigator.userAgent;